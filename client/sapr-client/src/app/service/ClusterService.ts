import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

@Injectable()
export class ClusterService {
  
  private basePath = "http://localhost:8080/cluster/";

  constructor(private http: HttpClient) {
  }

  standartClusterisation(data) {
    return this.http.post(this.basePath + 'fcm-standart', data);
  }
}
