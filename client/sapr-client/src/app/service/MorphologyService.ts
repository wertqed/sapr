/**
 * Created by VBelov on 21.12.2018.
 */
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export class MorphologyService {
  private basePath = "http://localhost:8080/morphology/";

  constructor(private http: HttpClient) {
  }

  analyzeText(text) {
    return this.http.post(this.basePath + 'analyze', text);
  }
}
