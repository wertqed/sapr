import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
@Injectable()
export class StatisticService {


  private basePath = "http://localhost:8080/statistic/";

  constructor(private http: HttpClient) {
  }

  getFrequency(data) {
    return this.http.post(this.basePath + 'frequency', data);
  }

  getMutualInformation(data) {
    return this.http.post(this.basePath + 'mutual-information', data);
  }
  getTscore(data) {
    return this.http.post(this.basePath + 't-score', data);
  }
  getLogLike(data) {
    return this.http.post(this.basePath + 'log-like', data);
  }
  getTfIdf(data) {
    return this.http.post(this.basePath + 'tf-idf', data);
  }
}
