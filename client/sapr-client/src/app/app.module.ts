import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatTabsModule } from '@angular/material';
import { AppComponent } from './app.component';
import { StatisticComponent } from './statistic/statistic.component';
import { MorphologyComponent } from './morphology/morphology.component';
import { ClusterComponent } from './cluster/cluster.component';
import { FormsModule } from '@angular/forms';
import {StatisticService} from "./service/StatisticService";
import {MorphologyService} from "./service/MorphologyService";
import {HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ClusterService} from "./service/ClusterService";

@NgModule({
  declarations: [
    AppComponent,
    StatisticComponent,
    MorphologyComponent,
    ClusterComponent,
  ],
  imports: [
    BrowserModule,
    MatTabsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    StatisticService,
    MorphologyService,
    ClusterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
