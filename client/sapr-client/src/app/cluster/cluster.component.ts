import {Component, OnInit} from '@angular/core';
import {ClusterService} from "../service/ClusterService";

@Component({
  selector: 'app-cluster',
  templateUrl: './cluster.component.html',
  styleUrls: ['./cluster.component.css']
})
export class ClusterComponent implements OnInit {

  private text = "";
  private termins = "";
  private analyzeResult = "";

  constructor(private clusterService: ClusterService) {
  }

  ngOnInit() {
  }

  clusterizeText() {
    let data = {
      text: this.text,
      termins: this.termins.split(" ")
    };
    let me = this;
    me.analyzeResult = "";
    this.clusterService.standartClusterisation(data).subscribe(res=> {
      res.forEach(function (word) {
        me.analyzeResult += "Слово: " + word.nGram + " Отношение к 1 кластеру: " + word.firstClusterProbability
          + " Отношение ко 2 кластеру: " + word.secondClusterProbability + "\n";
      });
    });
  }

}
