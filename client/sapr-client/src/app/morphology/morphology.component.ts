import {Component, OnInit} from '@angular/core';
import {MorphologyService} from "../service/MorphologyService";

@Component({
  selector: 'app-morphology',
  templateUrl: './morphology.component.html',
  styleUrls: ['./morphology.component.css']
})
export class MorphologyComponent implements OnInit {

  private text: "";
  private analyzeResult = "";

  constructor(private morphologyService: MorphologyService) {
  }

  ngOnInit() {
  }

  analyzeText() {
    var me = this;
    this.morphologyService.analyzeText(this.text).subscribe(res=> {
      me.analyzeResult = "Результат работы: \n";
      res.forEach(function (elem) {
        for (var key in elem) {
          me.analyzeResult += key + ": " + elem[key] + " ";
        }
        me.analyzeResult += "\n"
      });
    });
  }
}
