import {Component, OnInit} from '@angular/core';
import {StatisticService} from '../service/StatisticService';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {

  analyzeText: string = "";
  freqResult: string = "";
  mutualInformationText: string = "";
  mutualInformationResult: string = "";
  tScoreText: string = "";
  tScoreResult: string = "";
  logLikeText: string = "";
  logLikeResult: string = "";
  tfIdfText: string = "";
  tfIdfCorpus: string = "";
  tfIdfResult: string = "";

  corpus: string[] = [];

  constructor(private statisticService: StatisticService) {
  }

  ngOnInit() {
  }

  getFrequency() {
    var req = {
      text: this.analyzeText
    };
    this.statisticService.getFrequency(req).subscribe(res=> {
      this.freqResult = "";
      for (var key in res) {
        this.freqResult += key + ": " + res[key] + "\n";
      }
    });
  }

  getMutualInformation() {
    var req = {
      text: this.mutualInformationText
    };
    this.statisticService.getMutualInformation(req).subscribe(res=> {
      this.mutualInformationResult = "";
      this.mutualInformationResult += "Результат: \n";
      for (var key in res.resultMap) {
        this.mutualInformationResult += key + ": " + res.resultMap[key] + "\n";
      }
      this.mutualInformationResult += "\nОсновные b-граммы: " + res.mainBgrams.toString();
    });
  }

  getTScore() {
    var req = {
      text: this.tScoreText
    };
    this.statisticService.getTscore(req).subscribe(res=> {
      this.tScoreResult = "";
      this.tScoreResult += "Результат: \n";
      for (var key in res.resultMap) {
        this.tScoreResult += key + ": " + res.resultMap[key] + "\n";
      }
      this.tScoreResult += "\nОсновные b-граммы: " + res.mainBgrams.toString();
    });
  }

  getLogLike() {
    var req = {
      text: this.logLikeText
    };
    this.statisticService.getLogLike(req).subscribe(res=> {
      this.logLikeResult = "";
      this.logLikeResult += "Результат: \n";
      for (var key in res.resultMap) {
        this.logLikeResult += key + ": " + res.resultMap[key] + "\n";
      }
      this.logLikeResult += "\nОсновные b-граммы: " + res.mainBgrams.toString();
    });
  }

  getTfIfd() {
    var req = {
      text: this.logLikeText,
      corpus: this.corpus
    };
    this.statisticService.getTfIdf(req).subscribe(res=> {
      this.tfIdfResult = "";
      this.tfIdfResult += "Результат: \n";
      for (var key in res.resultMap) {
        this.tfIdfResult += key + ": " + res.resultMap[key] + "\n";
      }
      this.tfIdfResult += "\nОсновные b-граммы: " + res.mainBgrams.toString();
    });
  }

  addCorpus() {
    this.corpus.push(this.tfIdfCorpus);
    this.tfIdfCorpus = "";
  }
  cleanCorpus(){
    this.corpus = [];
    this.tfIdfCorpus = "";
  }
}
