package ru.vbelov.saprapp.dto.statistic;

import java.util.List;
import java.util.Map;

public class MutualInformationDTO {
    Map<String, Double> resultMap;
    List<String> mainBgrams;

    public Map<String, Double> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<String, Double> resultMap) {
        this.resultMap = resultMap;
    }

    public List<String> getMainBgrams() {
        return mainBgrams;
    }

    public void setMainBgrams(List<String> mainBgrams) {
        this.mainBgrams = mainBgrams;
    }
}
