package ru.vbelov.saprapp.dto.statistic;

import java.util.Map;

public class FrecuencyDto {

    private Map<String, Integer> frequency;

    public Map<String, Integer> getFrequency() {
        return frequency;
    }

    public void setFrequency(Map<String, Integer> frequency) {
        this.frequency = frequency;
    }
}
