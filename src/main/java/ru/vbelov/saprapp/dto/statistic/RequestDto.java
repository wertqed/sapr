package ru.vbelov.saprapp.dto.statistic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestDto {
    private String text;
    private String[] corpus;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String[] getCorpus() {
        return corpus;
    }

    public void setCorpus(String[] corpus) {
        this.corpus = corpus;
    }
}
