package ru.vbelov.saprapp.dto.claster;

public class ClasterReqDTO {
    private String[] termins;
    private String text;

    public String[] getTermins() {
        return termins;
    }

    public void setTermins(String[] termins) {
        this.termins = termins;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
