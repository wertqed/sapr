package ru.vbelov.saprapp.dto.claster;

public class ClusterModel {
    private String nGram;
    private double firstClusterProbability;
    private double secondClusterProbability;

    public ClusterModel(){

    }
    public ClusterModel(String nGram, double firstClusterProbability, double secondClusterProbability) {
        this.nGram = nGram;
        this.firstClusterProbability = firstClusterProbability;
        this.secondClusterProbability = secondClusterProbability;
    }

    public String getnGram() {

        return nGram;
    }

    public void setnGram(String nGram) {
        this.nGram = nGram;
    }

    public double getFirstClusterProbability() {
        return firstClusterProbability;
    }

    public void setFirstClusterProbability(double firstClusterProbability) {
        this.firstClusterProbability = firstClusterProbability;
    }

    public double getSecondClusterProbability() {
        return secondClusterProbability;
    }

    public void setSecondClusterProbability(double secondClusterProbability) {
        this.secondClusterProbability = secondClusterProbability;
    }
}
