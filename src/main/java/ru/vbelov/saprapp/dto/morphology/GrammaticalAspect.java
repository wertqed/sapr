package ru.vbelov.saprapp.dto.morphology;

public enum GrammaticalAspect {

    ipf("Несов в."), pf("Сов в.");

    private String text;

    GrammaticalAspect(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
