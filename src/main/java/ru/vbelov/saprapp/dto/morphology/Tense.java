package ru.vbelov.saprapp.dto.morphology;

public enum Tense {

    praes("Наст. вр."), inpraes("Буд. вр."), praet("Прош. вр.");

    private String text;

    Tense(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
