package ru.vbelov.saprapp.dto.morphology;

public enum Gender {

    m("М. р."), f("Ж. р."), n("Ср. р.");

    private String text;

    Gender(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
