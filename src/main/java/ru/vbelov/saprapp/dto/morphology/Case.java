package ru.vbelov.saprapp.dto.morphology;


public enum Case {

    nom("Им. п."), gen("Род. п."), dat("Дат п."), acc("Вин. п."), ins("Твор. п."), abl("Пред п."), part("Партитив"),
    loc("Мест. п."), voc("Зват. п.");

    private String text;

    Case(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
