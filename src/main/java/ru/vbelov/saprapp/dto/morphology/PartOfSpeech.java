package ru.vbelov.saprapp.dto.morphology;

public enum PartOfSpeech {

    A("Прилагательное"), ADV("Наречие"), ADVPRO("Местоименное наречие"), ANUM("Числительное-прилагательное"),
    APRO("Местоимение-прилагательное"), COM("Часть композита - сложного слова"), CONJ("Союз"), INTJ("Междометие"),
    NUM("Числительное"), PART("Частица"), PR("Предлог"), S("Существительное"), SPRO("Местоимение-существительное"),
    V("Глагол"), ger("Деепричастие"), partcp("Причастие");

    private String text;

    PartOfSpeech(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
