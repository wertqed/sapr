package ru.vbelov.saprapp.dto.morphology;

public enum Person {

    _1p("1-е л."), _2p("2-е л."), _3p("3-е л.");

    private String text;

    Person(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
