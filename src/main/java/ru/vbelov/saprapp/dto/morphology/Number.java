package ru.vbelov.saprapp.dto.morphology;

public enum Number {

    sg("Ед. ч"), pl("Мн. ч");

    private String text;

    Number(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
