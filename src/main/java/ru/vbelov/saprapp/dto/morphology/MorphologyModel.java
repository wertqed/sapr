package ru.vbelov.saprapp.dto.morphology;

public class MorphologyModel {
    private String word;
    private String lexeme;
    private String partOfSpeech;
    private String person;
    private String number;
    private String tense;
    private String gender;
    private String caseOfWord;
    private String grammaticalAspect;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getLexeme() {
        return lexeme;
    }

    public void setLexeme(String lexeme) {
        this.lexeme = lexeme;
    }

    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public void setPartOfSpeech(String partOfSpeech) {
        this.partOfSpeech = partOfSpeech;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTense() {
        return tense;
    }

    public void setTense(String tense) {
        this.tense = tense;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCaseOfWord() {
        return caseOfWord;
    }

    public void setCaseOfWord(String caseOfWord) {
        this.caseOfWord = caseOfWord;
    }

    public String getGrammaticalAspect() {
        return grammaticalAspect;
    }

    public void setGrammaticalAspect(String grammaticalAspect) {
        this.grammaticalAspect = grammaticalAspect;
    }
}
