package ru.vbelov.saprapp.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.vbelov.saprapp.dto.statistic.FrecuencyDto;
import ru.vbelov.saprapp.dto.statistic.MutualInformationDTO;
import ru.vbelov.saprapp.util.MapUtil;
import ru.vbelov.saprapp.util.TextUtils;

import java.util.*;

import static java.lang.Math.log;

@Service
public class StatisticServiceImpl implements StatisticService {
    @Override
    public Map<String, Integer> calculateFrequency(String text) {
        Map<String, Integer> frequency = new HashMap<>();
        String[] words = TextUtils.getWords(text);
        Arrays.stream(words).forEach(word -> {
            if (frequency.containsKey(word)) {
                Integer value = frequency.get(word);
                frequency.replace(word, value + 1);
            } else {
                frequency.put(word, 1);
            }
        });
        return frequency;
    }

    @Override
    public MutualInformationDTO calcMutualInformation(String text) {
        text = text.toLowerCase();
        MutualInformationDTO resultDTO = new MutualInformationDTO();
        String[] words = TextUtils.getWords(text);
        Map<String, Integer> freq = calculateFrequency(text);
        Map<String, Double> result = new HashMap<>();
        for (int i = 0; i < words.length - 1; i++) {
            String bGram = words[i] + " " + words[i + 1];
            Double fXY = getFxy(text, bGram);
            Double fX = Double.valueOf(freq.get(words[i]));
            Double fY = Double.valueOf(freq.get(words[i + 1]));
            Double MI = Math.log(fXY * words.length / (fX * fY)) / Math.log(2);
            result.put(bGram, MI);
        }
        resultDTO.setResultMap(result);
//        List<String> mainRes = new ArrayList<>();
//        result.forEach((bGram, val) -> {
//            if (val > 1) {
//                mainRes.add(bGram);
//            }
//        });
//        resultDTO.setMainBgrams(mainRes);
        Map<String, Double> sorted = MapUtil.sortByValue(result);
        List<String> mainRes = new ArrayList<>();
        int i = 0;
        for (Map.Entry<String, Double> res : sorted.entrySet()) {
            if (i == 9) {
                break;
            }
            mainRes.add(res.getKey());
            i++;
        }
        resultDTO.setMainBgrams(mainRes);
        return resultDTO;
    }

    @Override
    public MutualInformationDTO calcTscore(String text) {
        text = text.toLowerCase();
        MutualInformationDTO resultDTO = new MutualInformationDTO();
        String[] words = TextUtils.getWords(text);
        Map<String, Integer> freq = calculateFrequency(text);
        Map<String, Double> result = new HashMap<>();
        for (int i = 0; i < words.length - 1; i++) {
            String bGram = words[i] + " " + words[i + 1];
            Double fXY = getFxy(text, bGram);
            Double fX = Double.valueOf(freq.get(words[i]));
            Double fY = Double.valueOf(freq.get(words[i + 1]));
            Double Tscore = (fXY - fX * fY / words.length) / fXY;
            result.put(bGram, Tscore);
        }
        resultDTO.setResultMap(result);
        Map<String, Double> sorted = MapUtil.sortByValue(result);
        List<String> mainRes = new ArrayList<>();
        int i = 0;
        for (Map.Entry<String, Double> res : sorted.entrySet()) {
            if (i == 9) {
                break;
            }
            mainRes.add(res.getKey());
            i++;
        }
        resultDTO.setMainBgrams(mainRes);
        return resultDTO;
    }

    @Override
    public MutualInformationDTO calcLogLike(String text) {
        text = text.toLowerCase();
        MutualInformationDTO resultDTO = new MutualInformationDTO();
        String[] words = TextUtils.getWords(text);
        Map<String, Double> result = new HashMap<>();
        for (int i = 0; i < words.length - 1; i++) {
            String bGram = words[i] + " " + words[i + 1];
            Double a = getFxy(text, bGram);
            Double b = countBgrams(text, words[i]) - a;
            Double c = countBgrams(text, words[i + 1]) - a;
            Double d = countMainFreq(text) - a;
            Double logLike = a * log(a + 1) + b * log(b + 1) + c * log(c + 1) + d * log(d + 1) -
                    (a + b) * log(a + b + 1) - (a + c) * log(a + c + 1) - (b + d) * log(b + d + 1) -
                    (c + d) * log(c + d + 1) + (a + b + c + d) * log(a + b + c + d + 1);
            result.put(bGram, logLike);
        }
        resultDTO.setResultMap(result);
        Map<String, Double> sorted = MapUtil.sortByValue(result);
        List<String> mainRes = new ArrayList<>();
        int i = 0;
        for (Map.Entry<String, Double> res : sorted.entrySet()) {
            if (i == 9) {
                break;
            }
            mainRes.add(res.getKey());
            i++;
        }
        resultDTO.setMainBgrams(mainRes);
        return resultDTO;
    }

    @Override
    public MutualInformationDTO calcTfIdf(String text, String[] corpus) {
        MutualInformationDTO resultDTO = new MutualInformationDTO();
        String[] words = TextUtils.getWords(text);
        Map<String, Double> result = new HashMap<>();

        Map<String, Double> idfs = calculateIdf(words, corpus);
        Map<String, Integer> freq = calculateFrequency(text);
        for (String word : words) {
            double tf = (double) freq.get(word) / words.length;
            double idf = idfs.get(word);
            result.put(word, tf * idf);
        }
        resultDTO.setResultMap(result);
        Map<String, Double> sorted = MapUtil.sortByValue(result);
        List<String> mainRes = new ArrayList<>();
        int i = 0;
        for (Map.Entry<String, Double> res : sorted.entrySet()) {
            if (i == 9) {
                break;
            }
            mainRes.add(res.getKey());
            i++;
        }
        resultDTO.setMainBgrams(mainRes);
        return resultDTO;
    }

    private Map<String, Double> calculateIdf(String[] words, String[] corpus) {
        Map<String, Double> result = new HashMap<>();
        for (String word : words) {
            result.put(word, 0D);
        }
        for (String text : corpus) {
            String[] currWords = TextUtils.getWords(text);
            for (Map.Entry<String, Double> res : result.entrySet()) {
                if (Arrays.stream(currWords)
                        .filter(word -> word.equals(res.getKey())).findFirst().orElse(null) != null) {
                    res.setValue(res.getValue() + 1);
                }
            }

        }
        for (Map.Entry<String, Double> elem : result.entrySet()) {
            double idf = elem.getValue() == 0 ? 0 : log(corpus.length / elem.getValue());
            result.replace(elem.getKey(), idf);
        }
        return result;
    }

    private Double countBgrams(String text, String word) {
        String[] words = TextUtils.getWords(text);
        Double result = 0.0;
        for (int i = 0; i < words.length - 1; i++) {
            if (word.equals(words[i])) {
                String bGram = words[i] + " " + words[i + 1];
                result += getFxy(text, bGram);
            }
        }
        return result;
    }

    private Double countMainFreq(String text) {
        String[] words = TextUtils.getWords(text);
        Double result = 0.0;
        for (int i = 0; i < words.length - 1; i++) {
            String bGram = words[i] + " " + words[i + 1];
            result += getFxy(text, bGram);
        }
        return result;
    }

    private Double getFxy(String text, String bGram) {
        String[] wordsAll = TextUtils.getWords(text);
        String[] wordsNotXY = Arrays.stream(TextUtils.getWords(text.replaceAll(bGram, "")))
                .filter(word -> !StringUtils.isEmpty(word)).toArray(String[]::new);
        return ((double) wordsAll.length - (double) wordsNotXY.length) / 2;
    }
}
