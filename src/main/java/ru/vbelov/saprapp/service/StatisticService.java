package ru.vbelov.saprapp.service;

import org.springframework.util.StringUtils;
import ru.vbelov.saprapp.dto.statistic.FrecuencyDto;
import ru.vbelov.saprapp.dto.statistic.MutualInformationDTO;

import java.util.Map;

public interface StatisticService {

    Map<String, Integer> calculateFrequency(String text);

    MutualInformationDTO calcMutualInformation(String text);

    MutualInformationDTO calcTscore(String text);

    MutualInformationDTO calcLogLike(String text);

    MutualInformationDTO calcTfIdf(String text, String[] corpus);
}
