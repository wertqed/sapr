package ru.vbelov.saprapp.service.morfology;

import com.fasterxml.jackson.core.JsonParser;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Component;
import ru.vbelov.saprapp.dto.morphology.*;
import ru.vbelov.saprapp.dto.morphology.Number;

import java.util.HashSet;
import java.util.Set;

@Component
public class MyStemParser {

    private static final Set<String> validCases;

    private static final Set<String> validGenders;

    private static final Set<String> validGrammaticalAspects;

    private static final Set<String> validNumbers;

    private static final Set<String> validPartsOfSpeech;

    private static final Set<String> validPersons;

    private static final Set<String> validTenses;

    static {
        validCases = new HashSet<>();
        validGenders = new HashSet<>();
        validGrammaticalAspects = new HashSet<>();
        validNumbers = new HashSet<>();
        validPartsOfSpeech = new HashSet<>();
        validPersons = new HashSet<>();
        validTenses = new HashSet<>();

        for (Case _case : Case.values()) {
            validCases.add(_case.name());
        }
        for (Gender gender : Gender.values()) {
            validGenders.add(gender.name());
        }
        for (GrammaticalAspect aspect : GrammaticalAspect.values()) {
            validGrammaticalAspects.add(aspect.name());
        }
        for (Number number : Number.values()) {
            validNumbers.add(number.name());
        }
        for (PartOfSpeech partOfSpeech : PartOfSpeech.values()) {
            validPartsOfSpeech.add(partOfSpeech.name());
        }
        for (Person person : Person.values()) {
            validPersons.add(person.name());
        }
        for (Tense tense : Tense.values()) {
            validTenses.add(tense.name());
        }
    }

    private com.google.gson.JsonParser parser;

    public MyStemParser() {
        parser = new com.google.gson.JsonParser();
    }

    public MorphologyModel parseAnalyzeInfo(String line) {
        MorphologyModel result = new MorphologyModel();
        JsonObject json = parser.parse(line).getAsJsonObject();
        result.setWord(json.get("text").getAsString());
        JsonArray array = json.getAsJsonArray("analysis");
        for (JsonElement element : array) {
            JsonObject obj = element.getAsJsonObject();
            result.setLexeme(obj.get("lex").getAsString());
            parseDescription(obj.get("gr").getAsString(), result);
        }
        return result;
    }

    private void parseDescription(String description, MorphologyModel model) {
        String[] parts = description.split("[,=]");
        for (String part : parts) {
            if (part.startsWith("1") || part.startsWith("2") || part.startsWith("3")) {
                part = "_" + part;
            }
            if (validCases.contains(part)) {
                model.setCaseOfWord(Case.valueOf(part).getText());
            }
            if (validGenders.contains(part)) {
                model.setGender(Gender.valueOf(part).getText());
            }
            if (validGrammaticalAspects.contains(part)) {
                model.setGrammaticalAspect(GrammaticalAspect.valueOf(part).getText());
            }
            if (validNumbers.contains(part)) {
                model.setNumber(Number.valueOf(part).getText());
            }
            if (validPartsOfSpeech.contains(part)) {
                model.setPartOfSpeech(PartOfSpeech.valueOf(part).getText());
            }
            if (validPersons.contains(part)) {
                model.setPerson(Person.valueOf(part).getText());
            }
            if (validTenses.contains(part)) {
                model.setTense(Tense.valueOf(part).getText());
            }
        }
    }
}