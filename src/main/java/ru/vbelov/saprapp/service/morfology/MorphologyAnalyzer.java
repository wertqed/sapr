package ru.vbelov.saprapp.service.morfology;

import org.springframework.util.StringUtils;
import ru.vbelov.saprapp.dto.morphology.MorphologyModel;

import java.util.List;

public interface MorphologyAnalyzer {

    List<MorphologyModel> analyzeText(String text);
}
