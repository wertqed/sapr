package ru.vbelov.saprapp.service.morfology;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;
import ru.vbelov.saprapp.dto.morphology.MorphologyModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MorphologyAnalyzerImpl implements MorphologyAnalyzer{

    @Autowired
    private MyStemFactory myStemFactory;
    @Autowired
    private MyStemParser myStemParser;

    @Override
    public List<MorphologyModel> analyzeText(String text) {
        try {
            List<String> myStemRes = myStemFactory.analyzeText(text);
            List<MorphologyModel> result = new ArrayList<>();
            myStemRes.forEach(res->{
                result.add(myStemParser.parseAnalyzeInfo(res));
            });
            return result;
        } catch (IOException e) {

        } catch (MyStemApplicationException e) {
            e.printStackTrace();
        }

        return null;
    }
}
