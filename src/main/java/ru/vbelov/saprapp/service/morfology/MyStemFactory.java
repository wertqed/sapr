package ru.vbelov.saprapp.service.morfology;

import org.springframework.stereotype.Component;
import ru.stachek66.nlp.mystem.holding.Factory;
import ru.stachek66.nlp.mystem.holding.MyStem;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;
import ru.stachek66.nlp.mystem.holding.Request;
import scala.Option;
import scala.collection.JavaConversions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import ru.stachek66.nlp.mystem.model.Info;

@Component
public class MyStemFactory {

    private final static MyStem mystemAnalyzer =
            new Factory("-igd --eng-gr --format json --generate-all")
                    .newMyStem("3.0", Option.<File>empty()).get();


    public List<String> analyzeText(String text) throws IOException, MyStemApplicationException {
        List<String> res = new ArrayList<>();
        final Iterable<Info> result =
                JavaConversions.asJavaIterable(
                        mystemAnalyzer
                                .analyze(Request.apply(text))
                                .info()
                                .toIterable());

        for (final Info info : result) {
            System.out.println(info.initial() + " -> " + info.lex() + " | " + info.rawResponse());
            res.add(info.rawResponse());
        }
        return res;
    }

}
