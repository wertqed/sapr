package ru.vbelov.saprapp.service.claster;


import ru.vbelov.saprapp.dto.claster.ClusterModel;

import java.util.ArrayList;
import java.util.List;

public class FCM {

    private String[] labels; //термины

    private double[][] objects; // все объекты

    private double[][] probabilityMatrix; // матрица степеней принадлежности

    private double[][] clusterCenters; // центры кластеров

    private double fuzzyRate;   // параметр q

    private double epsilon;     // степень точности

    private int iterations; // количество итераций

    private int clusters; // массив центров кластеров

    public FCM(String[] labels, double[][] objects,
               double fuzzyRate, double epsilon, int iterations) {
        this.labels = labels;
        this.objects = objects;
        this.fuzzyRate = fuzzyRate;
        this.epsilon = epsilon;
        this.iterations = iterations;
        this.clusters = 2;
        this.clusterCenters = new double[clusters][objects[0].length];
        initializeProbabilityMatrix();
    }

    public List<ClusterModel> clustering() {
        List<ClusterModel> result = new ArrayList<>();
        int currentIteration = 0;
        double oldJ;
        double newJ = computeJ();
        do {
            oldJ = newJ;
            recalculateClusterCenters();
            recalculateProbabilityMatrix();
            newJ = computeJ();
            ++currentIteration;
        } while (Math.abs(oldJ - newJ) > epsilon && currentIteration < iterations);
        for (int i = 0; i < labels.length; ++i) {
            result.add(new ClusterModel(labels[i], probabilityMatrix[i][0], probabilityMatrix[i][1]));
        }
        return result;
    }

    private void initializeProbabilityMatrix() {
        probabilityMatrix = new double[objects.length][clusters];
        double right = 1.0 / clusterCenters.length;
        for (int i = 0; i < probabilityMatrix.length; ++i) {
            double sum = 0.0;
            for (int j = 0; j < probabilityMatrix[i].length; ++j) {
                probabilityMatrix[i][j] = Math.random() * right;
                sum += probabilityMatrix[i][j];
            }
            double ost = (1 - sum) / clusterCenters.length;
            for (int j = 0; j < clusterCenters.length; ++j) {
                probabilityMatrix[i][j] += ost;
            }
        }
    }

    private void recalculateClusterCenters() {
        for (int i = 0; i < clusterCenters.length; ++i) {
            double[] dividend = new double[clusterCenters[0].length];
            double divider = 0.0;
            for (int j = 0; j < objects.length; ++j) {
                double rate = Math.pow(probabilityMatrix[j][i], fuzzyRate);
                divider += rate;
                for (int k = 0; k < dividend.length; ++k) {
                    dividend[k] += objects[j][k] * rate;
                }
            }
            for (int k = 0; k < dividend.length; ++k) {
                clusterCenters[i][k] = dividend[k] / divider;
            }
        }
    }

    private void recalculateProbabilityMatrix() {
        double power = 2.0f / (fuzzyRate - 1.0f);
        for (int i = 0; i < probabilityMatrix.length; ++i) {
            for (int j = 0; j < probabilityMatrix[i].length; ++j) {
                double sum = 0;
                double dividend = computeDistance(objects[i], clusterCenters[j]);
                if (Math.abs(dividend) < 1e-7) {
                    for (int k = 0; k < clusterCenters.length; ++k) {
                        probabilityMatrix[i][k] = k == j ? 1 : 0;
                    }
                    break;
                }
                for (int k = 0; k < clusterCenters.length; ++k) {
                    double divider = computeDistance(objects[i], clusterCenters[k]);
                    sum += Math.pow(dividend / divider, power);
                }
                probabilityMatrix[i][j] = sum == 0 ? 0 : 1.0 / sum;
            }
        }
    }

    private double computeJ() {
        double sum = 0.0;
        for (int i = 0; i < probabilityMatrix.length; ++i) {
            for (int j = 0; j < probabilityMatrix[i].length; ++j) {
                sum += Math.pow(probabilityMatrix[i][j], fuzzyRate) * computeDistance(objects[i], clusterCenters[j]);
            }
        }
        return sum;
    }

    private double computeDistance(double[] x, double[] y) {
        double sum = 0.0;
        for (int i = 0; i < x.length; ++i) {
            sum += Math.pow(x[i] - y[i], 2);
        }
        return Math.sqrt(sum);
    }
}