package ru.vbelov.saprapp.util;

public class TextUtils {

    public static String normalizeText(String text){
        return text.toLowerCase().replaceAll("[^a-zA-Zа-яА-Я0-9ёЁ\\-]", " ");
    }

    public static String[] getWords(String text) {
        return text.toLowerCase().replaceAll("[^a-zA-Zа-яА-Я0-9ёЁ\\-]", " ").split("\\s+");
    }
}
