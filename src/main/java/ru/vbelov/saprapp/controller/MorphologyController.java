package ru.vbelov.saprapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;
import ru.vbelov.saprapp.dto.morphology.MorphologyModel;
import ru.vbelov.saprapp.service.morfology.MorphologyAnalyzer;
import ru.vbelov.saprapp.service.morfology.MyStemFactory;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "morphology")
public class MorphologyController {
    @Autowired
    private MorphologyAnalyzer morphologyAnalyzer;

    @RequestMapping(value = "analyze", method = RequestMethod.POST)
    public List<MorphologyModel> analizeText(@RequestBody String text) throws IOException, MyStemApplicationException {
        return morphologyAnalyzer.analyzeText(text);
    }
}
