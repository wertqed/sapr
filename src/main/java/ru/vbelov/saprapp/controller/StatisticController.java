package ru.vbelov.saprapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.vbelov.saprapp.dto.statistic.FrecuencyDto;
import ru.vbelov.saprapp.dto.statistic.MutualInformationDTO;
import ru.vbelov.saprapp.dto.statistic.RequestDto;
import ru.vbelov.saprapp.service.StatisticService;

import java.util.Map;

@RestController
@RequestMapping(value = "statistic")
public class StatisticController {

    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "frequency", method = RequestMethod.POST)
    public Map<String, Integer> calcFrequency(@RequestBody RequestDto requestDto){
        return statisticService.calculateFrequency(requestDto.getText());
    }

    @RequestMapping(value = "mutual-information", method = RequestMethod.POST)
    public MutualInformationDTO calcMutualInformation(@RequestBody RequestDto requestDto){
        return statisticService.calcMutualInformation(requestDto.getText());
    }

    @RequestMapping(value = "t-score", method = RequestMethod.POST)
    public MutualInformationDTO calcTscore(@RequestBody RequestDto requestDto){
        return statisticService.calcTscore(requestDto.getText());
    }

    @RequestMapping(value = "log-like", method = RequestMethod.POST)
    public MutualInformationDTO calcLogLike(@RequestBody RequestDto requestDto){
        return statisticService.calcLogLike(requestDto.getText());
    }

    @RequestMapping(value = "tf-idf", method = RequestMethod.POST)
    public MutualInformationDTO calcTfIfd(@RequestBody RequestDto requestDto){
        return statisticService.calcTfIdf(requestDto.getText(), requestDto.getCorpus());
    }
}
