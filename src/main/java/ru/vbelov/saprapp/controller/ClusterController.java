package ru.vbelov.saprapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.vbelov.saprapp.dto.claster.ClasterReqDTO;
import ru.vbelov.saprapp.dto.claster.ClusterModel;
import ru.vbelov.saprapp.service.StatisticService;
import ru.vbelov.saprapp.service.claster.FCM;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "cluster")
public class ClusterController {

    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "fcm-standart", method = RequestMethod.POST)
    public List<ClusterModel> getClasters(@RequestBody ClasterReqDTO clasterReqDTO) {
        Map<String, Integer> freq = statisticService.calculateFrequency(clasterReqDTO.getText());
        double[][] obj = new double[freq.size()][2];
        for (int i = 0; i < clasterReqDTO.getTermins().length; i++) {
            for (int j = 0; j < 2; j++) {
                obj[i][j] = freq.getOrDefault(clasterReqDTO.getTermins()[i], 0);
            }
        }
        return new FCM(clasterReqDTO.getTermins(), obj, 1.6, 0.001, 10000).clustering();
    }

}
