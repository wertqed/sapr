package ru.vbelov.saprapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaprAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaprAppApplication.class, args);
    }
}
